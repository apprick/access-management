package com.syncapse.store;

import com.syncapse.entity.Customer;
import com.syncapse.entity.Value;

public interface Store<T> {

	/**
	 * puts data into data store
	 * 
	 * @param customer
	 * @param val
	 */
	void put(final Customer customer, final Value<T> val);

	/**
	 * retrives the data from datastore
	 * 
	 * @param customer
	 * @return
	 */
	Value<T> get(final Customer customer);
}
