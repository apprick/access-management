package com.syncapse.store;

import java.util.HashMap;
import java.util.Map;

import com.syncapse.entity.Customer;
import com.syncapse.entity.Value;

/**
 * Non thread safe implementation of data store
 * 
 * @author rsingh
 * 
 */
public class DumbDataStore<T> implements Store<T> {

	private final Map<Customer, Value<T>> map = new HashMap<Customer, Value<T>>();

	public void put(final Customer customer, final Value<T> val) {
		this.map.put(customer, val);
	}

	public Value<T> get(final Customer customer) {
		return this.map.get(customer);
	}
}
