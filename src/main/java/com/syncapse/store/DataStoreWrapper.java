package com.syncapse.store;

import com.syncapse.concurrent.factory.LockFactory;
import com.syncapse.concurrent.factory.LockFactoryBuilder;
import com.syncapse.concurrent.factory.LockStrategy;
import com.syncapse.concurrent.lock.ConcurrentLock;
import com.syncapse.entity.Customer;
import com.syncapse.entity.Value;

public class DataStoreWrapper<T> implements Store<T> {

	private final Store<T> dataStore;
	private final LockFactory lockFactory;

	public DataStoreWrapper(final Store<T> dataStore, final LockStrategy strategy) {
		this.dataStore = dataStore;
		this.lockFactory = LockFactoryBuilder.getLockFactory(strategy);
	}

	public void put(final Customer customer, final Value<T> val) {
		final ConcurrentLock lock = this.lockFactory.getLock(customer);
		lock.lock(customer);
		try {
			this.dataStore.put(customer, val);
		} finally {
			lock.release(customer);
		}
	}

	public Value<T> get(final Customer customer) {
		final ConcurrentLock lock = this.lockFactory.getLock(customer);
		Value<T> val = null;
		lock.lock(customer);
		try {
			val = this.dataStore.get(customer);
		} finally {
			lock.release(customer);
		}
		return val;
	}
}
