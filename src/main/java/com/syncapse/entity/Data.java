package com.syncapse.entity;

/**
 * This represents a dumb data object
 * 
 * @author rsingh
 * 
 */
public class Data<T> implements Value<T> {
	private final T val;

	public Data(final T val) {
		this.val = val;
	}

	public T getVal() {
		return this.val;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.val == null) ? 0 : this.val.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Data other = (Data) obj;
		if (this.val == null) {
			if (other.val != null)
				return false;
		} else if (!this.val.equals(other.val))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Data [val=" + this.val + "]";
	}

}
