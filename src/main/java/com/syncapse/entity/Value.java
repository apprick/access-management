package com.syncapse.entity;

/**
 * interface to represent the value to be stored in the datastore
 * 
 * @author rsingh
 * 
 * @param <T>
 */
public interface Value<T> {

	T getVal();
}
