package com.syncapse.concurrent.factory;

import com.syncapse.concurrent.lock.BatchedLock;
import com.syncapse.concurrent.lock.ConcurrentLock;
import com.syncapse.entity.Customer;

public class BatchedLockFactory implements LockFactory {
	private final ConcurrentLock lock = new BatchedLock();

	public ConcurrentLock getLock(final Customer customer) {
		return this.lock;
	}

}
