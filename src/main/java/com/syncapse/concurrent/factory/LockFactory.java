package com.syncapse.concurrent.factory;

import com.syncapse.concurrent.lock.ConcurrentLock;
import com.syncapse.entity.Customer;

public interface LockFactory {

	/**
	 * returns the lock object based on the customr
	 * 
	 * @param customer
	 * @return
	 */
	ConcurrentLock getLock(Customer customer);
}
