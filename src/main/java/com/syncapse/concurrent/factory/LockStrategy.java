package com.syncapse.concurrent.factory;

public enum LockStrategy {
	ORDERED, BATCHED
}
