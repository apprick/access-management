package com.syncapse.concurrent.factory;

import java.util.concurrent.ConcurrentHashMap;

import com.syncapse.concurrent.lock.ConcurrentLock;
import com.syncapse.concurrent.lock.OrderedLock;
import com.syncapse.entity.Customer;

public class OrderedLockFactory implements LockFactory {

	private final ConcurrentHashMap<Customer, ConcurrentLock> lockMap = new ConcurrentHashMap<Customer, ConcurrentLock>();

	public ConcurrentLock getLock(final Customer customer) {
		ConcurrentLock lock = this.lockMap.get(customer);

		if (lock != null)
			return lock;

		final ConcurrentLock newLock = new OrderedLock();
		lock = this.lockMap.putIfAbsent(customer, newLock);

		return lock == null ? newLock : lock;
	}

}
