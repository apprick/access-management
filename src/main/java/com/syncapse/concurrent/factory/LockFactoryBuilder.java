package com.syncapse.concurrent.factory;

import java.util.HashMap;
import java.util.Map;

/**
 * creates lock factory based on locking strategy
 * 
 * @author rsingh
 * 
 */
public class LockFactoryBuilder {
	private static LockFactory orderedLockFactory;
	private static LockFactory batchedLockFactory;

	/**
	 * This is a little enhancement which can enable the user to plug in his/her
	 * own locking strategy by implementing its own locking factory. Ofcourse we
	 * need to make our locking strategy enum class pluggable to incorporate the
	 * new locking pattern, but in this implementation i have not done that for
	 * simplicity sake.
	 */
	private static Map<LockStrategy, LockFactory> lockFactories = new HashMap<LockStrategy, LockFactory>();

	public static LockFactory getLockFactory(final LockStrategy strategy) {
		switch (strategy) {
		case BATCHED:
			return getBatchedLockFactory();

		case ORDERED:
			return getOrderedLockFactory();

		default:
			final LockFactory factory = lockFactories.get(strategy);
			if (factory != null)
				return factory;

			throw new RuntimeException("invalud strategy :" + strategy);
		}
	}

	public static void registerLockFactory(final LockStrategy strategy,
			final LockFactory lockFactory) {
		lockFactories.put(strategy, lockFactory);
	}

	/**
	 * we could simply do non lazy initialization here for simplicity, but in
	 * general factories are expensive to create so doing lazy initialization
	 * 
	 * @return
	 */
	private static LockFactory getOrderedLockFactory() {
		if (orderedLockFactory != null)
			return orderedLockFactory;

		synchronized (LockFactoryBuilder.class) {
			if (orderedLockFactory == null)
				orderedLockFactory = new OrderedLockFactory();
		}
		return orderedLockFactory;
	}

	private static LockFactory getBatchedLockFactory() {
		if (batchedLockFactory != null)
			return batchedLockFactory;

		synchronized (LockFactoryBuilder.class) {
			if (batchedLockFactory == null)
				batchedLockFactory = new BatchedLockFactory();
		}
		return batchedLockFactory;
	}
}
