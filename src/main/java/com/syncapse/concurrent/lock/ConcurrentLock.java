package com.syncapse.concurrent.lock;

import com.syncapse.entity.Customer;

public interface ConcurrentLock {

	/**
	 * acquires a lock
	 */
	void lock(Customer customer);

	/**
	 * releases the lock
	 */
	void release(Customer customer);
}
