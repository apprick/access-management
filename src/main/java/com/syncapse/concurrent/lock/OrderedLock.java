package com.syncapse.concurrent.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.syncapse.entity.Customer;

public class OrderedLock implements ConcurrentLock {
	private final Lock lock = new ReentrantLock();

	public void lock(final Customer customer) {
		this.lock.lock();
	}

	public void release(final Customer customer) {
		this.lock.unlock();
	}

}
