package com.syncapse.concurrent.lock;

import java.util.concurrent.atomic.AtomicInteger;

import com.syncapse.entity.Customer;

public class BatchedLock implements ConcurrentLock {
	private final Object waitObject = new Object();
	private Customer lockHeldBy;
	private final AtomicInteger customersHoldingLocks = new AtomicInteger(0);

	public void lock(final Customer customer) {
		synchronized (this.waitObject) {
			if (this.lockHeldBy == null || this.lockHeldBy.equals(customer)) {
				this.lockHeldBy = customer;
				this.customersHoldingLocks.incrementAndGet();
				return;
			} else {
				try {
					// spurious wake up will not be a problem since even if the
					// thread wakes up for no reason it will try to grab the
					// lock
					this.waitObject.wait();

					// try to grab a lock again when wake up
					lock(customer);
				} catch (final InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

		}

	}

	public void release(final Customer customer) {
		if (!customer.equals(this.lockHeldBy))
			return;

		if (this.customersHoldingLocks.intValue() > 0)
			this.customersHoldingLocks.decrementAndGet();

		if (this.customersHoldingLocks.intValue() == 0) {
			synchronized (this.waitObject) {

				// to avoid race condition between lock and release
				if (this.customersHoldingLocks.intValue() == 0) {
					this.lockHeldBy = null;
					this.waitObject.notifyAll();
				}
			}
		}

	}

}
