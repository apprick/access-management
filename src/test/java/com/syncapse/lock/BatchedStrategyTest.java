package com.syncapse.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.syncapse.concurrent.factory.LockStrategy;
import com.syncapse.entity.Customer;
import com.syncapse.store.DataStoreWrapper;

/**
 * The test only tests the lock mechanism. I have not added any data validation
 * tests. Ideally for such a system there should be some kind of data validation
 * of get and put. Since this exercise is basically meant for implementing
 * various locking strategies so i am not testing the actual put and get in the
 * data store
 * 
 * Also the test uses wait and notify mechanism to simulate work under the lock.
 * This has a problem of rare condition of spurious wake up. The test has to be
 * made more concrete so that it doesn't fail for the wrong reason
 * 
 * @author rsingh
 * 
 */
public class BatchedStrategyTest {
	private MockOrderedStore<String> dataStore;
	private DataStoreWrapper<String> store;
	private Object waitObject;

	@Before
	public void init() {
		this.waitObject = new Object();
		this.dataStore = new MockOrderedStore<String>(this.waitObject);
		this.store = new DataStoreWrapper<String>(this.dataStore, LockStrategy.BATCHED);
	}

	@Test
	public void testEnterSameCustomer() throws Exception {
		final Customer customer = new Customer("customer");

		final int numOfCalls = 100;
		final CountDownLatch latch = new CountDownLatch(numOfCalls);
		final List<Thread> ths = new ArrayList<Thread>();
		final AtomicInteger waitingThreads = new AtomicInteger(0);

		for (int i = 0; i < numOfCalls; i++) {
			final Thread th = new Thread(new Runnable() {

				public void run() {
					waitingThreads.incrementAndGet();
					latch.countDown();
					BatchedStrategyTest.this.store.put(customer, null);
					waitingThreads.decrementAndGet();
				}
			});
			ths.add(th);
			th.start();
		}

		// wait till all threads start
		latch.await();

		// wait for some time to let all the threads try to enter the critical
		// section
		Thread.sleep(2000);

		Assert.assertEquals(numOfCalls, waitingThreads.intValue());
		Assert.assertEquals(numOfCalls, this.dataStore.getCriticalSectionCount());

		// let all thread complete
		synchronized (this.waitObject) {
			this.waitObject.notifyAll();
		}

		// this will ensure that none of the threads kept waiting to enter the
		// critical section
		for (final Thread th : ths)
			th.join();
	}

	@Test
	/**
	 * Test for testing a scenario when multiple customers have multiple threads asking for a lock
	 * @throws Exception
	 */
	public void testWaitForCustomer() throws Exception {
		final Customer customer = new Customer("customer");

		final int numOfCallsForFirstCustomer = 15;
		final CountDownLatch latch = new CountDownLatch(numOfCallsForFirstCustomer);
		final List<Thread> ths = new ArrayList<Thread>();
		final AtomicInteger waitingThreads = new AtomicInteger(0);

		for (int i = 0; i < numOfCallsForFirstCustomer; i++) {
			final Thread th = new Thread(new Runnable() {

				public void run() {
					waitingThreads.incrementAndGet();
					latch.countDown();
					BatchedStrategyTest.this.store.put(customer, null);
					waitingThreads.decrementAndGet();
				}
			});
			ths.add(th);
			th.start();
		}

		// wait till all threads start
		latch.await();

		// wait for some time to let all the threads try to enter the critical
		// section
		Thread.sleep(2000);

		Assert.assertEquals(numOfCallsForFirstCustomer, waitingThreads.intValue());
		Assert.assertEquals(numOfCallsForFirstCustomer, this.dataStore.getCriticalSectionCount());

		final int numOfOtherCustomers = 20;
		final int numOfCallPerCustomer = 30;
		final List<Customer> customers = new ArrayList<Customer>();
		final CountDownLatch latch1 = new CountDownLatch(numOfOtherCustomers);

		for (int i = 0; i < numOfOtherCustomers; i++)
			customers.add(new Customer("customer-" + i));

		for (final Customer cust : customers) {
			for (int i = 0; i < numOfCallPerCustomer; i++) {
				final Thread th = new Thread(new Runnable() {

					public void run() {
						waitingThreads.incrementAndGet();
						latch1.countDown();
						BatchedStrategyTest.this.store.put(cust, null);
						waitingThreads.decrementAndGet();
					}
				});
				ths.add(th);
				th.start();
			}
		}

		// wait till all threads start
		latch1.await();

		// wait for some time to let all the threads try to enter the critical
		// section
		Thread.sleep(2000);

		final int numOfWaitingThreads = numOfCallsForFirstCustomer + numOfOtherCustomers
				* numOfCallPerCustomer;
		Assert.assertEquals(numOfWaitingThreads, waitingThreads.intValue());
		Assert.assertEquals(numOfCallsForFirstCustomer, this.dataStore.getCriticalSectionCount());

		for (int i = 1; i <= numOfOtherCustomers; i++) {
			// let all thread complete put operation
			synchronized (this.waitObject) {
				this.waitObject.notifyAll();
			}

			// sleep for some time to let a thread process to get into the
			// critical section
			Thread.sleep(100);
			Assert.assertEquals(numOfCallsForFirstCustomer + i * numOfCallPerCustomer,
					this.dataStore.getCriticalSectionCount());
			Assert.assertEquals(numOfWaitingThreads - numOfCallsForFirstCustomer - (i - 1)
					* numOfCallPerCustomer, waitingThreads.intValue());
		}

		// let the last thread complete
		synchronized (this.waitObject) {
			this.waitObject.notifyAll();
		}

		Thread.sleep(1000);
		Assert.assertEquals(
				numOfCallsForFirstCustomer + numOfOtherCustomers * numOfCallPerCustomer,
				this.dataStore.getCriticalSectionCount());
		Assert.assertEquals(0, waitingThreads.intValue());

		// this will ensure that none of the threads kept waiting to enter the
		// critical section
		for (final Thread th : ths)
			th.join();
	}
}
