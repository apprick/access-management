package com.syncapse.lock;

import java.util.concurrent.atomic.AtomicInteger;

import com.syncapse.entity.Customer;
import com.syncapse.entity.Value;
import com.syncapse.store.Store;

public class MockOrderedStore<T> implements Store<T> {
	final private Object waitObject;
	private Customer lastLockHeloBy;
	final private AtomicInteger criticalSectionCount;

	public MockOrderedStore(final Object waitObject) {
		this.waitObject = waitObject;
		this.criticalSectionCount = new AtomicInteger();
	}

	public void put(final Customer customer, final Value<T> val) {
		this.lastLockHeloBy = customer;
		this.criticalSectionCount.incrementAndGet();
		synchronized (this.waitObject) {

			try {
				this.waitObject.wait();
			} catch (final InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	public Value<T> get(final Customer customer) {
		this.lastLockHeloBy = customer;
		this.criticalSectionCount.incrementAndGet();
		try {
			this.waitObject.wait();
		} catch (final InterruptedException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	public int getCriticalSectionCount() {
		return this.criticalSectionCount.intValue();
	}

	public Customer getLastLockHeloBy() {
		return this.lastLockHeloBy;
	}

}
