package com.syncapse.lock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import com.syncapse.concurrent.factory.LockStrategy;
import com.syncapse.entity.Customer;
import com.syncapse.store.DataStoreWrapper;

/**
 * The test only tests the lock mechanism. I have not added any data validation
 * tests. Ideally for such a system there should be some kind of data validation
 * of get and put. Since this exercise is basically meant for implementing
 * various locking strategies so i am not testing the actual put and get in the
 * data store
 * 
 * Also the test uses wait and notify mechanism to simulate work under the lock.
 * This has a problem of rare condition of spurious wake up. The test has to be
 * made more concrete so that it doesn't fail for the wrong reason
 * 
 * 
 * @author rsingh
 * 
 */
public class OrderedStrategyTest {
	private MockOrderedStore<String> dataStore;
	private DataStoreWrapper<String> store;
	private Object waitObject;

	@Before
	public void init() {
		this.waitObject = new Object();
		this.dataStore = new MockOrderedStore<String>(this.waitObject);
		this.store = new DataStoreWrapper<String>(this.dataStore, LockStrategy.ORDERED);
	}

	@Test
	public void testEnterPerCustomer() throws Exception {

		final List<Customer> customers = new ArrayList<Customer>();
		final List<Thread> ths = new ArrayList<Thread>();

		final int numOfCustomers = 10;
		final CountDownLatch latch = new CountDownLatch(numOfCustomers);
		final AtomicInteger waitingThreads = new AtomicInteger(0);

		for (int i = 0; i < numOfCustomers; i++)
			customers.add(new Customer("customer-" + i));

		for (final Customer customer : customers) {
			final Thread th = new Thread(new Runnable() {

				public void run() {
					waitingThreads.incrementAndGet();
					latch.countDown();
					OrderedStrategyTest.this.store.put(customer, null);
					waitingThreads.decrementAndGet();
				}
			});
			ths.add(th);
			th.start();
		}

		// wait till all the threads start
		latch.await();
		// wait for some time to let all the threads enter the critical section
		Thread.sleep(2000);

		Assert.assertEquals(numOfCustomers, waitingThreads.intValue());
		Assert.assertEquals(numOfCustomers, this.dataStore.getCriticalSectionCount());

		// this should let all the threads to process put and exit
		synchronized (this.waitObject) {
			this.waitObject.notifyAll();
		}

		// wait for some time to let all the threads finish processing
		Thread.sleep(2000);
		Assert.assertEquals(0, waitingThreads.intValue());

		// this will ensure that none of the threads kept waiting to enter the
		// critical section
		for (final Thread th : ths)
			th.join();

	}

	@Test
	public void testBlockSameCustomer() throws Exception {
		final Customer customer = new Customer("customer");

		final int numOfCalls = 100;
		final CountDownLatch latch = new CountDownLatch(numOfCalls);
		final List<Thread> ths = new ArrayList<Thread>();
		final AtomicInteger waitingThreads = new AtomicInteger(0);

		for (int i = 0; i < numOfCalls; i++) {
			final Thread th = new Thread(new Runnable() {

				public void run() {
					waitingThreads.incrementAndGet();
					latch.countDown();
					OrderedStrategyTest.this.store.put(customer, null);
					waitingThreads.decrementAndGet();
				}
			});
			ths.add(th);
			th.start();
		}

		// wait till all threads start
		latch.await();

		// wait for some time to let all the threads try to enter the critical
		// section
		Thread.sleep(2000);

		Assert.assertEquals(numOfCalls, waitingThreads.intValue());
		Assert.assertEquals(1, this.dataStore.getCriticalSectionCount());

		for (int i = 1; i < numOfCalls; i++) {
			synchronized (this.waitObject) {
				this.waitObject.notify();
			}

			Thread.sleep(100);
			Assert.assertEquals(numOfCalls - i, waitingThreads.intValue());
			Assert.assertEquals(i + 1, this.dataStore.getCriticalSectionCount());
		}

		// let the last thread complete
		synchronized (this.waitObject) {
			this.waitObject.notify();
		}

		// this will ensure that none of the threads kept waiting to enter the
		// critical section
		for (final Thread th : ths)
			th.join();
	}
}
